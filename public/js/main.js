$( document ).ready(function() {
    $('#formModal').modal();
    $('#editFormModal').modal();
    $('.collapsible').collapsible();

    $('.add-recipe').on('click', function () {
        $('#formModal').modal('open');
    });

    $('.edit-recipe').on('click', function () {
        $('#edit-form-id').val($(this).data('id'));
        $('#edit-form-name').val($(this).data('name'));
        $('#edit-form-ingredientes').val($(this).data('ingredientes'));
        $('#edit-form-direccion').val($(this).data('direccion'));
        $('#editFormModal').modal('open');
    })


    $('.delete-recipe').click(function() {
        var id = $(this).data('id');
        var url = '/delete/'+id;
        if (confirm('Eliminar Recipe?')) {
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (result) {
                    console.log('Eliminando...');
                    window.location.href='/';
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }
    });
});
