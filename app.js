var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cons = require('consolidate'),
    dust = require('dustjs-helpers'),
    pg = require('pg'),
    app = express();

const pool = new pg.Pool({
      user: 'postgres',
      host: 'localhost',
      database: 'recipesdb',
      password: 'postgres',
      port: 5432,
  });

app.engine('dust', cons.dust);

// Default Ext. dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set carpeta publica
app.use(express.static(path.join(__dirname, 'public')));

// Body parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

var index = require('./routes/index');

app.use('/', index);

// Server
app.listen(80, function(){
    console.log('Server iniciado..');
});
