var express = require('express');
var router = express.Router();

var pg = require('pg');
const pool = new pg.Pool({
      user: 'postgres',
      host: 'localhost',
      database: 'recipesdb',
      password: 'postgres',
      port: 5432,
  });

router.get('/', function(req, res){
        pool.query('SELECT * FROM recipes ORDER BY name ASC', function(err, result) {
            if (err) {
                return console.error('error en query', err);
            }
            res.render('index', {recipes: result.rows});
        });
});

router.post('/add', function(req, res) {
    pool.query('INSERT INTO recipes(name, ingredientes, direccion) VALUES ($1, $2, $3)',
                [req.body.name, req.body.ingredientes, req.body.direccion],
                function(err) {
                    if (err) {
                        return console.log('error en query', err);
                    }
                    res.redirect('/');
                }
    );
});

router.post('/edit', function(req, res) {
    pool.query('UPDATE recipes SET name=$1, ingredientes=$2, direccion=$3 WHERE id = $4',
                [req.body.name, req.body.ingredientes, req.body.direccion, req.body.id],
                function(err) {
                    if (err) {
                        return console.log('error en query', err);
                    }
                    res.redirect('/');
                }
    );
});

router.delete('/delete/:id', function (req, res) {
    pool.query('DELETE FROM recipes WHERE id=$1', [req.params.id],
        function (err) {
            if (err) {
                return console.log('Error en query', err);
            }
            res.send('200');
        }
    );
});
module.exports = router;
